#!/bin/bash

source $DOCKER_ANTX_PATH/config.sh

if  $RUN_CAMERA ; then
    echo "Launch Camera Docker container"
    if ! screen -list | grep -q "camera"; then
        screen -dmS camera $DOCKER_ANTX_PATH/docker-camera/run_container.sh
        echo "container started"
    else
        echo "container already running"
    fi
fi

if  $RUN_FOXGLOVE ; then
    echo "Launch Foxglove Docker container"
    if ! screen -list | grep -q "foxglove"; then
        screen -dmS foxglove $DOCKER_ANTX_PATH/docker-foxglove/run_container.sh
        echo "container started"
    else
        echo "container already running"
    fi
fi

if  $RUN_MAVROS ; then
    echo "Launch MAVROS Docker container"
    if ! screen -list | grep -q "mavros"; then
        screen -dmS mavros $DOCKER_ANTX_PATH/docker-mavros/run_container.sh
        echo "container started"
    else
        echo "container already running"
    fi
fi

if  $RUN_VRPN ; then
    echo "Launch VRPN Docker container"
    if ! screen -list | grep -q "vrpn"; then
        screen -dmS vrpn $DOCKER_ANTX_PATH/docker-vrpn/run_container.sh
        echo "container started"
    else
        echo "container already running"
    fi
fi
