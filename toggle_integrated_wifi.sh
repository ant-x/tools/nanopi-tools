#!/bin/bash

FILE="/etc/modprobe.d/blacklist-nanopiair.conf"
STRING="blacklist brcmfmac"
RESTART_NEEDED=false

CONNECTION_NAME="IntegratedConnection"

# Check if the script is run as root
if [ "$EUID" -ne 0 ]; then
    echo "This script must be run as root."
    exit 1
fi

# Check command-line arguments
if [ "$1" == "--disable" ]; then
    # Add the string to the file if it doesn't exist
    if ! grep -q "$STRING" "$FILE"; then
        echo "$STRING" | sudo tee -a "$FILE"
        echo "Integrated WiFi has been disabled. The changes will take effect after restarting NanoPi NEO Air."
        RESTART_NEEDED=true

        # Remove existing client connection if present
        nmcli connection delete "$CONNECTION_NAME" >/dev/null 2>&1

    else
        echo "Integrated WiFi is already disabled."
    fi
elif [ "$1" == "--enable" ]; then
    # Remove the string from the file if present
    if grep -q "$STRING" "$FILE"; then
        sudo sed -i "/$STRING/d" "$FILE"
        echo "Integrated WiFi has been enabled. The changes will take effect after restarting NanoPi NEO Air."
        RESTART_NEEDED=true
    else
        echo "Integrated WiFi is already enabled."
    fi
else
    echo "Usage: sudo $0 --disable   Disable integrated WiFi (requires restart)"
    echo "       sudo $0 --enable    Enable integrated WiFi (requires restart)"
    exit 1
fi

# Print a message to restart if changes were made
if [ "$RESTART_NEEDED" == true ]; then
    echo "Please restart NanoPi NEO Air to apply changes."
fi
