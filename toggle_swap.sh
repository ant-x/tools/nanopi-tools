#!/bin/bash

# Check if the script is run as root
if [ "$EUID" -ne 0 ]; then
    echo "This script must be run as root."
    exit 1
fi

turn_on_swap() {
    if [ ! -f "/swap" ]; then
        echo "Creating swap file..."
        sudo dd if=/dev/zero of=/swap bs=1M count=1024
        sudo mkswap /swap
        sudo chmod 0600 /swap
    fi

    echo "Turning on swap..."
    sudo swapon /swap

    echo "Add swap entry to /etc/fstab..."
    sudo sed -i '/\/swap/d' /etc/fstab
    echo "/swap swap swap defaults 0 0" | sudo tee -a /etc/fstab

    echo "Setting swappiness to 100 from /etc/sysctl.conf..."
    sudo sed -i '/^vm.swappiness=/d' /etc/sysctl.conf
    echo "vm.swappiness=100" | sudo tee -a /etc/sysctl.conf
    sudo sysctl -p

    echo "Swap turned on successfully."
}

turn_off_swap() {
    echo "Turning off swap..."
    sudo swapoff /swap

    echo "Removing swap entry from /etc/fstab..."
    sudo sed -i '/\/swap/d' /etc/fstab

    echo "Removing swap space..."
    sudo rm -f /swap

    echo "Setting swappiness to 0 from /etc/sysctl.conf..."
    sudo sed -i '/^vm.swappiness=/d' /etc/sysctl.conf
    echo "vm.swappiness=0" | sudo tee -a /etc/sysctl.conf
    sudo sysctl -p

    echo "Swap turned off successfully."
}

print_help(){
    echo "Usage: sudo $0 --on   Activate additional swap space"
    echo "       sudo $0 --off  Deactivate additional swap space"
    exit 1
}

case "$1" in
    --on)
        turn_on_swap
        ;;
    --off)
        turn_off_swap
        ;;
    *)
        print_help
        exit 1
        ;;
esac

exit 0
