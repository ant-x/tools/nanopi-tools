#!/bin/bash

export NANOPI_TOOLS_PATH=/home/antx/nanopi-tools

interface_old=$(cat /home/antx/.interface_registered)
interface_read=$(ip link show | awk '/wlx/ {print $2}' | tr -d :)

echo "read = '$interface_read', old = '$interface_old'" > /home/antx/.log.txt

if [ "$interface_old" = "$interface_read" ]
then
    echo "The network adapter has not changed" >> /home/antx/.log.txt
else
    echo "The network adapter has changed, turn on hotspot mode" >> /home/antx/.log.txt
    echo $($NANOPI_TOOLS_PATH/toggle_client_hotspot.sh --hotspot) >> /home/antx/.log.txt
fi
