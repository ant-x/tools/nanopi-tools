#!/bin/bash

export NANOPI_TOOLS_PATH=/home/antx/nanopi-tools
source $NANOPI_TOOLS_PATH/network_config.sh

# Check if the script is run as root
if [ "$EUID" -ne 0 ]; then
    echo "This script must be run as root."
    exit 1
fi

# Create a new connection using nmcli
nmcli connection add type wifi ifname wlan0 \
                                con-name "$client_connection_name" \
                                autoconnect yes \
                                ssid "$client_ssid" \
                                mode infrastructure \
                                802-11-wireless-security.key-mgmt wpa-psk \
                                802-11-wireless-security.psk "$client_psk"

# Connect as client
nmcli connection up "$client_connection_name"